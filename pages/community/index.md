---
title: Community
description: 🧑🏿‍🤝‍🧑🏻 Get support, give support, join the conversation!
toc: true
---

Welcome to the CalyxOS community. Please see our [[code-of-conduct]], [[pledge]] and dive right in.

## Guidelines
* Be kind. Don't be snarky. Have curious conversation; don't cross-examine. Please don't fulminate. Please don't sneer, including at the rest of the community.
* Comments should get more thoughtful and substantive, not less, as a topic gets more divisive.
* When disagreeing, please reply to the argument instead of calling names. "That is idiotic; 1 + 1 is 2, not 3" can be shortened to "1 + 1 is 2, not 3."
* Please respond to the strongest plausible interpretation of what someone says, not a weaker one that's easier to criticize. Assume good faith.
* Eschew flamebait. Avoid unrelated controversies and generic tangents.
* Please don't post shallow dismissals, especially of other people's work. A good critical comment teaches us something.
* Assume good faith. (It cannot be said enough)

## Chat

All below channels are on Matrix. They are public and unencrypted. You can click the link to directly connect to the channel, or using one of the [clients](https://matrix.org/clients/).

| Name | Purpose | Matrix (Primary) | Telegram |
| ---- | ------- | ------ | -------- | --- |
| CalyxOS | Main channel, support for CalyxOS | [#calyxos:matrix.org](https://app.element.io/#/room/#CalyxOS:matrix.org) | [CalyxOSpublic](https://t.me/CalyxOSpublic) |
| CalyxOS Testers | For testing CalyxOS builds early and providing feedback | [#calyxos-testers:matrix.org](https://app.element.io/#/room/#calyxos-testers:matrix.org) | [CalyxOSTesters](https://t.me/CalyxOSTesters) |
| CalyxOS Off-topic | For all off-topic conversations | [#calyxos-offtopic:matrix.org](https://app.element.io/#/room/#calyxos-offtopic:matrix.org) | [CalyxOSOfftopic](https://t.me/CalyxOSOffTopic) |
| SeedVault Backup | For SeedVault related discussion | [#seedvault-app:matrix.org](https://app.element.io/#/room/#seedvault-app:matrix.org) | [SeedVaultApp](https://t.me/SeedVaultApp) |
| CalyxOS Development | For development related discussion | [#calyxos-dev:matrix.org](https://app.element.io/#/room/#calyxos-dev:matrix.org) | [CalyxOSDev](https://t.me/CalyxOSDev) |
| CalyxOS Documentation | For documentation / translation related discussion | [#calyxos-docs:matrix.org](https://app.element.io/#/room/#calyxos-docs:matrix.org) | [CalyxOSDocs](https://t.me/CalyxOSDocs) |
| CalyxOS Infrastructure | For infrastructure related discussion | [#calyxos-infra:matrix.org](https://app.element.io/#/room/#calyxos-infra:matrix.org) | [CalyxOSInfra](https://t.me/CalyxOSInfra) |
| CalyxOS Feeds | Live feed of code commits / GitLab activity / Reddit posts | [#calyxos-feeds:matrix.org](https://app.element.io/#/room/#calyxos-feeds:matrix.org) | [CalyxOSFeeds](https://t.me/CalyxOSFeeds) |
| Calyx Institute | For discussions about the Calyx Institute | [#calyx:matrix.org](https://app.element.io/#/room/#calyx:matrix.org) | [CalyxInstitute](https://t.me/CalyxInstitute) |

### Bridges for the chat channels

Matrix is the primary medium of communication. Telegram is bridged to Matrix, which means all messages get sent to both places!

* **Telegram**: Bridged using [mautrix-telegram](https://matrix.org/docs/projects/bridge/mautrix-telegram)
  * On Matrix, Telegram messages will appear just like any other message.
  * On Telegram, Matrix messages will appear to be sent by "Calyx Matrix Bridge" -> `@SepalBridgeBot`
* **IRC**:
  * Bridging has been disabled as of 2023-11-03

### Matrix Space

Matrix has a new [spaces feature](https://element.io/blog/spaces-the-next-frontier/), which lets you view all these rooms together.

We've created a [CalyxOS space](https://app.element.io/#/room/#calyxos-space:matrix.org).

## Stay Up To Date

### Mastodon

* [@calyxos@fosstodon.org](https://fosstodon.org/@calyxos)
* [@calyxinstitute@mastodon.social](https://mastodon.social/@calyxinstitute)

### Twitter

* [@CalyxOS](https://twitter.com/CalyxOS)
* [@calyxinsitute](https://twitter.com/calyxinstitute)

### Reddit

* [r/CalyxOS](https://www.reddit.com/r/CalyxOS/)